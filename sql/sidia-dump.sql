--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.26
-- Dumped by pg_dump version 11.9 (Debian 11.9-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: sidia; Type: SCHEMA; Schema: -; Owner: db202f15
--

CREATE SCHEMA sidia;


ALTER SCHEMA sidia OWNER TO db202f15;

--
-- Name: batch_pengiriman_asal_check(); Type: FUNCTION; Schema: sidia; Owner: db202f15
--

CREATE FUNCTION sidia.batch_pengiriman_asal_check() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN 
 IF NOT EXISTS (SELECT w.id_lokasi 
   from batch_pengiriman bp, warehouse_provinsi w, faskes f, permohonan_sumber_daya_faskes psd, petugas_faskes pf
   where w.id_lokasi = NEW.id_lokasi_asal AND w.id_lokasi = bp.id_lokasi_asal AND w.id_lokasi = f.id_lokasi AND pf.username = f.username_petugas AND pf.username = psd.username_petugas_faskes)
 THEN
   RAISE EXCEPTION '% adalah id lokasi asal tidak sesuai ketentuan, silahkan cek kembali data yang ingin ditambahkan ', NEW.id_lokasi_asal;
 END IF;
 RETURN NEW;
 END
$$;


ALTER FUNCTION sidia.batch_pengiriman_asal_check() OWNER TO db202f15;

--
-- Name: batch_pengiriman_tujuan_check(); Type: FUNCTION; Schema: sidia; Owner: db202f15
--

CREATE FUNCTION sidia.batch_pengiriman_tujuan_check() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN 
 IF NOT EXISTS (SELECT bp.id_lokasi_tujuan 
   from batch_pengiriman bp, faskes f, permohonan_sumber_daya_faskes psd, petugas_faskes pf
   where f.id_lokasi = NEW.id_lokasi_tujuan AND pf.username = f.username_petugas AND pf.username = psd.username_petugas_faskes)
 THEN
   RAISE EXCEPTION '% adalah id lokasi tujuan tidak sesuai ketentuan, silahkan cek kembali data yang ingin ditambahkan ', NEW.id_lokasi_tujuan;
 END IF;
 RETURN NEW;
 END
$$;


ALTER FUNCTION sidia.batch_pengiriman_tujuan_check() OWNER TO db202f15;

--
-- Name: check_stok_permohonan(); Type: FUNCTION; Schema: sidia; Owner: db202f15
--

CREATE FUNCTION sidia.check_stok_permohonan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE admin VARCHAR(20);

 BEGIN
    SELECT username FROM ADMIN_SATGAS INTO admin
    ORDER BY RANDOM()
    LIMIT 1;

    IF (SELECT SUM(di.jumlah_item) FROM DAFTAR_ITEM di, STOK_WAREHOUSE_PROVINSI swp, FASKES f
        WHERE NEW.no_transaksi_sumber_daya = di.no_transaksi_sumber_daya
        AND NEW.username_petugas_faskes = f.username_petugas
        AND swp.kode_item_sumber_daya = di.kode_item_sumber_daya
        AND f.id_lokasi = swp.id_lokasi_warehouse) > (SELECT SUM(swp.jumlah)
        FROM DAFTAR_ITEM di, STOK_WAREHOUSE_PROVINSI swp, FASKES f
        WHERE NEW.no_transaksi_sumber_daya = di.no_transaksi_sumber_daya
        AND NEW.username_petugas_faskes = f.username_petugas
        AND f.id_lokasi = swp.id_lokasi_warehouse
        AND swp.kode_item_sumber_daya = di.kode_item_sumber_daya)

    THEN
        INSERT INTO RIWAYAT_STATUS_PERMOHONAN VALUES
        ('WAI', NEW.no_transaksi_sumber_daya, admin, (SELECT CURRENT_DATE));
        RAISE EXCEPTION 'Sumber daya yang anda pesan belum tersedia di STOK_WAREHOUSE';

    END IF;

    INSERT INTO RIWAYAT_STATUS_PERMOHONAN VALUES
    ('REQ', NEW.no_transaksi_sumber_daya, admin,(SELECT CURRENT_DATE));

    UPDATE DAFTAR_ITEM SET harga_kumulatif = (SELECT (di.jumlah_item * isd.harga_satuan)
    FROM DAFTAR_ITEM di, ITEM_SUMBER_DAYA isd
    WHERE NEW.no_transaksi_sumber_daya = di.no_transaksi_sumber_daya
    AND di.kode_item_sumber_daya = isd.kode),
    berat_kumulatif = (SELECT (di.jumlah_item * isd.berat_satuan)
    FROM DAFTAR_ITEM di, ITEM_SUMBER_DAYA isd
    WHERE NEW.no_transaksi_sumber_daya = di.no_transaksi_sumber_daya
    AND di.kode_item_sumber_daya = isd.kode)
    WHERE NEW.no_transaksi_sumber_daya = no_transaksi_sumber_daya;

    UPDATE TRANSAKSI_SUMBER_DAYA SET total_item = (SELECT SUM(di.jumlah_item)
    FROM DAFTAR_ITEM DI WHERE NEW.no_transaksi_sumber_daya = di.no_transaksi_sumber_daya ),
    total_berat = (SELECT SUM(di.berat_kumulatif)
    FROM DAFTAR_ITEM DI WHERE NEW.no_transaksi_sumber_daya = di.no_transaksi_sumber_daya)
    WHERE NEW.no_transaksi_sumber_daya = nomor;

    RETURN NEW;
 END
$$;


ALTER FUNCTION sidia.check_stok_permohonan() OWNER TO db202f15;

--
-- Name: username_pengguna_check(); Type: FUNCTION; Schema: sidia; Owner: db202f15
--

CREATE FUNCTION sidia.username_pengguna_check() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
 IF EXISTS (SELECT 1 FROM PENGGUNA WHERE username = NEW.username)
 THEN
 RAISE EXCEPTION '% untuk username pengguna telah digunakan, silahkan mendaftar menggunakan username yang lain ', NEW.username;
 END IF;
 RETURN NEW;
 END
$$;


ALTER FUNCTION sidia.username_pengguna_check() OWNER TO db202f15;

--
-- Name: validate_pesanan_sumber_daya(); Type: FUNCTION; Schema: sidia; Owner: db202f15
--

CREATE FUNCTION sidia.validate_pesanan_sumber_daya() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
  INSERT INTO RIWAYAT_STATUS_PESANAN VALUES('REQ-SUP', NEW.nomor_pesanan,
        (SELECT isd.username_supplier FROM ITEM_SUMBER_DAYA isd, DAFTAR_ITEM di
        WHERE NEW.nomor_pesanan = di.no_transaksi_sumber_daya AND di.kode_item_sumber_daya = isd.kode), (SELECT CURRENT_DATE));

  UPDATE DAFTAR_ITEM SET harga_kumulatif = (SELECT (di.jumlah_item * isd.harga_satuan) FROM DAFTAR_ITEM di, ITEM_SUMBER_DAYA isd
        WHERE NEW.nomor_pesanan = di.no_transaksi_sumber_daya AND di.kode_item_sumber_daya = isd.kode),
        berat_kumulatif = (SELECT (di.jumlah_item * isd.berat_satuan) FROM DAFTAR_ITEM di, ITEM_SUMBER_DAYA isd
        WHERE NEW.nomor_pesanan = di.no_transaksi_sumber_daya AND di.kode_item_sumber_daya = isd.kode)
        WHERE NEW.nomor_pesanan = no_transaksi_sumber_daya;

  UPDATE TRANSAKSI_SUMBER_DAYA SET total_item = (SELECT SUM(di.jumlah_item) FROM DAFTAR_ITEM DI
        WHERE NEW.nomor_pesanan = di.no_transaksi_sumber_daya),
        total_berat = (SELECT SUM(di.berat_kumulatif) FROM DAFTAR_ITEM DI
        WHERE NEW.nomor_pesanan = di.no_transaksi_sumber_daya)
  WHERE NEW.nomor_pesanan = nomor;

        RAISE NOTICE 'Supplier yang akan menyediakan item adalah %', (SELECT isd.username_supplier FROM ITEM_SUMBER_DAYA isd, DAFTAR_ITEM di
        WHERE NEW.nomor_pesanan = di.no_transaksi_sumber_daya AND di.kode_item_sumber_daya = isd.kode);

  RETURN NEW;
 END
$$;


ALTER FUNCTION sidia.validate_pesanan_sumber_daya() OWNER TO db202f15;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_satgas; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.admin_satgas (
    username character varying(50) NOT NULL
);


ALTER TABLE sidia.admin_satgas OWNER TO db202f15;

--
-- Name: batch_pengiriman; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.batch_pengiriman (
    kode character varying(5) NOT NULL,
    username_satgas character varying(50) NOT NULL,
    username_petugas_distribusi character varying(50),
    tanda_terima character varying(30) NOT NULL,
    nomor_transaksi_sumber_daya integer NOT NULL,
    id_lokasi_asal character varying(5) NOT NULL,
    id_lokasi_tujuan character varying(5) NOT NULL,
    no_kendaraan character varying(10) NOT NULL
);


ALTER TABLE sidia.batch_pengiriman OWNER TO db202f15;

--
-- Name: daftar_item; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.daftar_item (
    no_transaksi_sumber_daya integer NOT NULL,
    no_urut integer NOT NULL,
    jumlah_item integer NOT NULL,
    kode_item_sumber_daya character varying(5) NOT NULL,
    berat_kumulatif integer,
    harga_kumulatif integer
);


ALTER TABLE sidia.daftar_item OWNER TO db202f15;

--
-- Name: faskes; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.faskes (
    kode_faskes_nasional character varying(15) NOT NULL,
    id_lokasi character varying(5) NOT NULL,
    username_petugas character varying(50) NOT NULL,
    kode_tipe_faskes character varying(3) NOT NULL
);


ALTER TABLE sidia.faskes OWNER TO db202f15;

--
-- Name: item_sumber_daya; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.item_sumber_daya (
    kode character varying(5) NOT NULL,
    username_supplier character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    harga_satuan integer NOT NULL,
    berat_satuan integer NOT NULL,
    kode_tipe_item character varying(3) NOT NULL
);


ALTER TABLE sidia.item_sumber_daya OWNER TO db202f15;

--
-- Name: kendaraan; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.kendaraan (
    nomor character varying(10) NOT NULL,
    nama character varying(30) NOT NULL,
    jenis_kendaraan character varying(15) NOT NULL,
    berat_maksimum integer NOT NULL
);


ALTER TABLE sidia.kendaraan OWNER TO db202f15;

--
-- Name: lokasi; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.lokasi (
    id character varying(5) NOT NULL,
    provinsi character varying(20) NOT NULL,
    kabkot character varying(20) NOT NULL,
    kecamatan character varying(20) NOT NULL,
    kelurahan character varying(20) NOT NULL,
    jalan_no character varying(20) NOT NULL
);


ALTER TABLE sidia.lokasi OWNER TO db202f15;

--
-- Name: manifes_item; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.manifes_item (
    kode_batch_pengiriman character varying(5) NOT NULL,
    no_urut integer NOT NULL,
    kode_item_sumber_daya character varying(5) NOT NULL,
    jumlah_item integer NOT NULL,
    berat_kumulatif integer
);


ALTER TABLE sidia.manifes_item OWNER TO db202f15;

--
-- Name: pengguna; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.pengguna (
    username character varying(50) NOT NULL,
    password character varying(20) NOT NULL,
    nama character varying(50) NOT NULL,
    alamat_kel character varying(20) NOT NULL,
    alamat_kec character varying(20) NOT NULL,
    alamat_kabkot character varying(20) NOT NULL,
    alamat_prov character varying(20) NOT NULL,
    no_telepon character varying(15) NOT NULL
);


ALTER TABLE sidia.pengguna OWNER TO db202f15;

--
-- Name: permohonan_sumber_daya_faskes; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.permohonan_sumber_daya_faskes (
    no_transaksi_sumber_daya integer NOT NULL,
    username_petugas_faskes character varying(50) NOT NULL,
    catatan text
);


ALTER TABLE sidia.permohonan_sumber_daya_faskes OWNER TO db202f15;

--
-- Name: pesanan_sumber_daya; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.pesanan_sumber_daya (
    nomor_pesanan integer NOT NULL,
    username_admin_satgas character varying(50) NOT NULL,
    total_harga integer
);


ALTER TABLE sidia.pesanan_sumber_daya OWNER TO db202f15;

--
-- Name: petugas_distribusi; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.petugas_distribusi (
    username character varying(50) NOT NULL,
    no_sim character varying(15) NOT NULL
);


ALTER TABLE sidia.petugas_distribusi OWNER TO db202f15;

--
-- Name: petugas_faskes; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.petugas_faskes (
    username character varying(50) NOT NULL
);


ALTER TABLE sidia.petugas_faskes OWNER TO db202f15;

--
-- Name: riwayat_status_pengiriman; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.riwayat_status_pengiriman (
    kode_status_batch_pengiriman character varying(3) NOT NULL,
    kode_batch character varying(5) NOT NULL,
    tanggal timestamp without time zone NOT NULL
);


ALTER TABLE sidia.riwayat_status_pengiriman OWNER TO db202f15;

--
-- Name: riwayat_status_permohonan; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.riwayat_status_permohonan (
    kode_status_permohonan character varying(3) NOT NULL,
    nomor_permohonan integer NOT NULL,
    username_admin character varying(50),
    tanggal date NOT NULL
);


ALTER TABLE sidia.riwayat_status_permohonan OWNER TO db202f15;

--
-- Name: riwayat_status_pesanan; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.riwayat_status_pesanan (
    kode_status_pesanan character varying(7) NOT NULL,
    no_pesanan integer NOT NULL,
    username_supplier character varying(50) NOT NULL,
    tanggal date NOT NULL
);


ALTER TABLE sidia.riwayat_status_pesanan OWNER TO db202f15;

--
-- Name: status_batch_pengiriman; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.status_batch_pengiriman (
    kode character varying(3) NOT NULL,
    nama character varying(25) NOT NULL
);


ALTER TABLE sidia.status_batch_pengiriman OWNER TO db202f15;

--
-- Name: status_permohonan; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.status_permohonan (
    kode character varying(3) NOT NULL,
    nama character varying(25) NOT NULL
);


ALTER TABLE sidia.status_permohonan OWNER TO db202f15;

--
-- Name: status_pesanan; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.status_pesanan (
    kode character varying(7) NOT NULL,
    nama character varying(25) NOT NULL
);


ALTER TABLE sidia.status_pesanan OWNER TO db202f15;

--
-- Name: stok_faskes; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.stok_faskes (
    kode_faskes character varying(15) NOT NULL,
    kode_item_sumber_daya character varying(5) NOT NULL,
    jumlah integer NOT NULL
);


ALTER TABLE sidia.stok_faskes OWNER TO db202f15;

--
-- Name: stok_warehouse_provinsi; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.stok_warehouse_provinsi (
    id_lokasi_warehouse character varying(5) NOT NULL,
    jumlah integer NOT NULL,
    kode_item_sumber_daya character varying(5) NOT NULL
);


ALTER TABLE sidia.stok_warehouse_provinsi OWNER TO db202f15;

--
-- Name: supplier; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.supplier (
    username character varying(50) NOT NULL,
    nama_organisasi character varying(20) NOT NULL
);


ALTER TABLE sidia.supplier OWNER TO db202f15;

--
-- Name: tipe_faskes; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.tipe_faskes (
    kode character varying(3) NOT NULL,
    nama_tipe character varying(30) NOT NULL
);


ALTER TABLE sidia.tipe_faskes OWNER TO db202f15;

--
-- Name: tipe_item; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.tipe_item (
    kode character varying(3) NOT NULL,
    nama character varying(50) NOT NULL
);


ALTER TABLE sidia.tipe_item OWNER TO db202f15;

--
-- Name: transaksi_sumber_daya; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.transaksi_sumber_daya (
    nomor integer NOT NULL,
    tanggal date NOT NULL,
    total_berat integer,
    total_item integer
);


ALTER TABLE sidia.transaksi_sumber_daya OWNER TO db202f15;

--
-- Name: warehouse_provinsi; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.warehouse_provinsi (
    id_lokasi character varying(5) NOT NULL
);


ALTER TABLE sidia.warehouse_provinsi OWNER TO db202f15;

--
-- Name: warehouse_supplier; Type: TABLE; Schema: sidia; Owner: db202f15
--

CREATE TABLE sidia.warehouse_supplier (
    id_lokasi character varying(5) NOT NULL,
    username_supplier character varying(50) NOT NULL
);


ALTER TABLE sidia.warehouse_supplier OWNER TO db202f15;

--
-- Data for Name: admin_satgas; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.admin_satgas (username) FROM stdin;
bletcherous
begpervert
jumpydull
samflyvirgo
plutocatloce
vinhofish
reiwaffles
\.


--
-- Data for Name: batch_pengiriman; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.batch_pengiriman (kode, username_satgas, username_petugas_distribusi, tanda_terima, nomor_transaksi_sumber_daya, id_lokasi_asal, id_lokasi_tujuan, no_kendaraan) FROM stdin;
B-001	jumpydull	brucelee	Udah diterima nih	1	l-3	l-5	B1562DB
B-002	begpervert	popocamber	Accepted	2	l-1	l-3	B8294KL
B-003	jumpydull	ajishoo	Sudah sampai nih	3	l-7	l-9	B1562DB
B-004	samflyvirgo	kelsdieo	Wah udah sampai	4	l-9	l-6	B2193GH
B-005	plutocatloce	ajishoo	Halo sudah ya	5	l-8	l-1	B1231JI
B-006	begpervert	popocamber	Accepted	2	l-1	l-6	B8294KL
\.


--
-- Data for Name: daftar_item; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.daftar_item (no_transaksi_sumber_daya, no_urut, jumlah_item, kode_item_sumber_daya, berat_kumulatif, harga_kumulatif) FROM stdin;
1	1	80	apd-1	80	7200000
2	2	100	apd-2	100	10000000
3	3	3	ven-2	75	81000000
4	4	55	vak-1	110	115500000
5	5	100	rap-1	100	70000000
6	6	10	pcr-1	20	50000000
7	7	5	bed-2	60	6500000
8	8	150	msk-2	150	13500000
9	9	4	ven-1	120	100000000
10	10	100	rap-3	100	72000000
11	11	50	apd-2	50	5000000
12	12	50	apd-1	50	4500000
13	13	50	apd-2	50	5000000
15	15	50	apd-2	50	5000000
\.


--
-- Data for Name: faskes; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.faskes (kode_faskes_nasional, id_lokasi, username_petugas, kode_tipe_faskes) FROM stdin;
faskes-1	l-1	phdrcovil	RUJ
faskes-2	l-3	dllieblbaum	PUS
faskes-3	l-5	shprdaspnwll	RUJ
faskes-4	l-7	cthylnsch	IDD
faskes-5	l-9	lbbvll	KLI
\.


--
-- Data for Name: item_sumber_daya; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.item_sumber_daya (kode, username_supplier, nama, harga_satuan, berat_satuan, kode_tipe_item) FROM stdin;
apd-1	avictormarchington	Alat Pelindung Diri PT MM	90000	1	APD
apd-2	akmxon	Alat Pelindung Diri PT HK	100000	1	APD
ven-1	avictormarchington	Ventilator PT MM	25000000	30	VEN
ven-2	akmxon	Ventilator PT HK	27000000	25	VEN
bed-1	vhlmshllke	Tempat Tidur PT SS	1500000	15	BED
bed-2	avictormarchington	Tempat Tidur PT MM	1300000	15	BED
msk-1	vhlmshllke	Paket Masker PT SS	100000	1	MSK
msk-2	adlaibr	Paket Masker PT CD	90000	1	MSK
vak-1	dnstbbins	Paket Vaksin PT MC	2100000	2	VAK
vak-2	arglebargle	Paket Vaksin PT SM	2500000	2	VAK
pcr-1	dnstbbins	PCR Test Kit PT MC	5000000	2	PCR
pcr-2	vhlmshllke	PCR Test Kit PT SS	4800000	2	PCR
rap-1	akmxon	Rapid Test Kit PT HK	700000	1	RAP
rap-2	adlaibr	Rapid Test Kit PT CD	750000	1	RAP
rap-3	arglebargle	Rapid Test Kit PT SM	720000	1	RAP
\.


--
-- Data for Name: kendaraan; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.kendaraan (nomor, nama, jenis_kendaraan, berat_maksimum) FROM stdin;
B1562DB	Toyoti Sienna	Van	1500
B9782KB	Toyoti Hiace Van	Van	2000
B8294KL	Handa Scoopie Stylish	Scooter	500
B2193GH	Handa Scoopie Prestige	Scooter	550
B1231JI	Toyoti Succeed Van	Van	1800
\.


--
-- Data for Name: lokasi; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.lokasi (id, provinsi, kabkot, kecamatan, kelurahan, jalan_no) FROM stdin;
l-1	Jawa Barat	Depok	Beji	Pondok Cina	123
l-2	DKI JAKARTA	JAKARTA BARAT	Cengkareng	Kapuk	234
l-3	Yogyakarta	Kulon Progo	Sentolo	Banguncipto	345
l-4	DKI JAKARTA	JAKARTA TIMUR	Pulogadung	Rawamangun	456
l-5	Jawa Timur	Kediri	Gurah	Adan-Adan	567
l-6	Jawa Tengah	Kebumen	Kebumen	Adikarso	678
l-7	Jawa Barat	Bogor	Tanjungsari	Antajaya	789
l-8	DKI JAKARTA	JAKARTA UTARA	Tanjung Priok	Sunter Jaya	101
l-9	Jawa Tengah	Klateng	Wedi	Birit	102
l-10	Jawa Barat	Tasikmalaya	Pancatengah	Cibongas	202
\.


--
-- Data for Name: manifes_item; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.manifes_item (kode_batch_pengiriman, no_urut, kode_item_sumber_daya, jumlah_item, berat_kumulatif) FROM stdin;
B-001	1	apd-1	80	80
B-002	2	apd-2	100	100
B-003	3	ven-2	3	75
B-004	4	vak-1	55	110
B-005	5	rap-1	100	100
B-002	6	pcr-1	10	20
B-003	7	bed-2	5	60
B-003	8	msk-2	150	150
B-005	9	ven-1	4	120
B-001	10	rap-3	100	100
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.pengguna (username, password, nama, alamat_kel, alamat_kec, alamat_kabkot, alamat_prov, no_telepon) FROM stdin;
avictormarchington	ZAGxn4wwn	Avictor Marchington	Bojonggaling	Bantargadung	SUKABUMI	JAWA BARAT	088583006315
akmxon	7thuWrGLH	Akim Mixon	Cideng	Gambir	JAKARTA PUSAT	DKI JAKARTA	080247717153
vhlmshllke	LDEvE5EUt	Vilhelmina Shillaker	Tugu Utara	Koja	JAKARTA UTARA	DKI JAKARTA	082786193556
dnstbbins	nvJUbvx6Z	Deny Stubbins	Benteng Utara	Benteng	KEPULAUAN SLAYAR	SULAWESI SELATAN	088066085359
arglebargle	7ha2fKKSa	Arglome Bargle	Padalarang	Padalarang	BANDUNG	JAWA BARAT082734652521
bletcherous	cQYtrM27e	Bletcy Hereous	Duri Kosambi	Cengkareng	JAKARTA BARAT	DKI JAKARTA	087815860201
begpervert	4s9uWyNg9	Beggy Pervert	Gogorea	Waeapo	BURU	MALUKU	082915170504
jumpydull	S7wWzZBwt	Jumno Pydull	Menteng Dalam	Tebet	JAKARTA SELATAN	DKI JAKART084646762107
samflyvirgo	C5K8zQdSU	Sammy Flyvirgo	Tobati	Jayapura Selatan	JAYAPURA	PAPUA	087509029001
plutocatloce	5FQpngKvq	Plutocas Carloce	Pure	Wakorumba Selatan	MUNA	SULAWESI TENGGARA	081897161651
vinhofish	gHE46JrD6	Vinho Meifish	Sumalangka	Tondano Utara	MINAHASA	SULAWESI UTARA	085254897354
reiwaffles	R6AycYSKb	Rein Waffles	Kamal Muara	Penjaringan	JAKARTA UTARA	DKI JAKARTA	087482458257
riurocketice	7Wk68VvXK	Fredi Avrahamy	Cibinong	Cibinong	BOGOR	JAWA BARAT083513139501
cnstanceskggin	eX4efTwMq	Constance Skoggins	Mbali Lendeiki	Ndao Nuse	ROTE NDAO	NUSA TENGGARA TIMUR	083727700202
odwyer	r7Y9Rshuy	Ode Dowyer	Jelambar	Grogol Petamburan	JAKARTA BARAT	DKI JAKARTA	081374103305
kdncernld	cFsJ938Zk	Kandace Ranald	Andalas	Padang Timur	PADANG	SUMATERA BARAT	080805376708
blrsltrs	yg9jLTDVR	Blair Salters	Muara Sahung	Muara Sahung	KAUR	BENGKULU	084974416011
phdrcovil	8qydVGB2G	Phaedra Covil	Karet	Setiabudi	JAKARTA SELATAN	DKI JAKART086911420152
elssasclly	5bEnPHSfn	Elissa Scally	Candi Jaya	Dempo Tengah	PAGAR ALAM	SUMATERA SELATAN	084862376955
dllieblbaum	A9AQhUyVu	Dollie Bleibaum	Apung	Tanjung Selor	BULUNGAN	KALIMANTAN UTARA	082301333858
gbccra	z85fDEKcy	Gui Beccera	Melinggih	Payangan	GIANYAR	BALI	080992289511
shprdaspnwll	jy6jg9B5E	Shepard Aspinwall	Rawamangun	Pulogadung	JAKARTA TIMUR	DKI JAKARTA	085490308103
lbbvll	Qk2amYvzm	Lebbie Ovell	Cikeusal	Palimanan	CIREBON	JAWA BARAT	087609916706
cthylnsch	ePkF6CYbL	Cathyleen Schirach	Alalak Utara	Banjarmasin Utara	BANJARMASIN	KALIMANTAN SELATAN	086355805909
adlaibr	L2x23jS83	Adlai Brear	Utan Kayu Utara	Matraman	JAKARTA TIMUR	DKI JAKART083587977021
brucelee	dJwc2bSjd	Bruce Lee	Pondok Cina	Beji	DEPOK	JAWA BARAT	081128812812
popocamber	skac1928C	Amber Popov	Kelapa Gading Timur	Kelapa Gading	JAKARTA UTARA	DKI JAKARTA	081293819212
ajishoo	hcs27Ska9	Aji Shookiman	Gondoriyo	Jambu	SEMARANG	JAWA TENGAH	089812729211
kelsdieo	indowd921	Kelsey Dio	Kemayoran	Krembangan	SURABAYA	JAWA TIMUR	087819283012
\.


--
-- Data for Name: permohonan_sumber_daya_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.permohonan_sumber_daya_faskes (no_transaksi_sumber_daya, username_petugas_faskes, catatan) FROM stdin;
1	phdrcovil	\N
2	elssasclly	\N
3	dllieblbaum	\N
4	gbccra	\N
5	shprdaspnwll	\N
12	phdrcovil	\N
13	dllieblbaum	\N
14	dllieblbaum	\N
\.


--
-- Data for Name: pesanan_sumber_daya; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.pesanan_sumber_daya (nomor_pesanan, username_admin_satgas, total_harga) FROM stdin;
1	bletcherous	2300000
2	begpervert	565000
3	vinhofish	1119000
4	plutocatloce	440000
5	samflyvirgo	1290000
11	samflyvirgo	150000
15	samflyvirgo	150000
\.


--
-- Data for Name: petugas_distribusi; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.petugas_distribusi (username, no_sim) FROM stdin;
riurocketice	987686546323
cnstanceskggin	764326483723
odwyer	875426712342
kdncernld	24683046403
blrsltrs	765488325466
brucelee	729839218921
popocamber	981298391022
ajishoo	829922102938
kelsdieo	928122017213
\.


--
-- Data for Name: petugas_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.petugas_faskes (username) FROM stdin;
phdrcovil
elssasclly
dllieblbaum
gbccra
shprdaspnwll
lbbvll
cthylnsch
\.


--
-- Data for Name: riwayat_status_pengiriman; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.riwayat_status_pengiriman (kode_status_batch_pengiriman, kode_batch, tanggal) FROM stdin;
HLG	B-005	2020-01-01 00:42:05
PRO	B-002	2020-01-01 01:48:43
DLV	B-003	2020-01-01 11:59:43
DLV	B-004	2020-01-01 13:22:29
RET	B-005	2020-01-02 03:27:55
HLG	B-004	2020-01-03 20:37:10
DLV	B-002	2020-01-04 10:36:18
PRO	B-003	2020-01-05 22:04:28
RET	B-004	2020-01-05 23:51:17
DLV	B-005	2020-01-06 00:16:22
HLG	B-002	2020-01-06 06:39:26
PRO	B-001	2020-01-05 05:41:39
DLV	B-001	2020-01-06 00:42:05
RET	B-001	2020-01-07 01:48:43
PRO	B-005	2020-01-08 11:59:43
PRO	B-004	2020-01-08 13:22:29
OTW	B-001	2020-01-09 03:27:55
OTW	B-002	2020-01-09 20:37:10
OTW	B-004	2020-01-10 10:36:18
OTW	B-005	2020-01-10 22:04:28
\.


--
-- Data for Name: riwayat_status_permohonan; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.riwayat_status_permohonan (kode_status_permohonan, nomor_permohonan, username_admin, tanggal) FROM stdin;
REQ	1	bletcherous	2020-01-03
REJ	1	begpervert	2020-01-06
PRO	2	jumpydull	2020-01-07
REJ	2	samflyvirgo	2020-01-04
MAS	3	bletcherous	2020-01-09
FIN	3	plutocatloce	2020-01-11
MAS	4	vinhofish	2020-01-02
REQ	4	reiwaffles	2020-01-08
REJ	5	samflyvirgo	2020-01-12
PRO	5	samflyvirgo	2020-01-01
REQ	12	begpervert	2021-05-20
REQ	13	plutocatloce	2021-05-20
REQ	14	vinhofish	2021-05-20
\.


--
-- Data for Name: riwayat_status_pesanan; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.riwayat_status_pesanan (kode_status_pesanan, no_pesanan, username_supplier, tanggal) FROM stdin;
REQ-SUP	1	avictormarchington	2020-01-04
REJ-SUP	1	akmxon	2020-01-06
PRO-SUP	2	vhlmshllke	2020-01-07
REJ-SUP	2	akmxon	2020-01-09
MAS-SUP	3	dnstbbins	2020-01-11
FIN-SUP	3	arglebargle	2020-01-03
MAS-SUP	4	adlaibr	2020-01-08
REQ-SUP	4	adlaibr	2020-01-02
REJ-SUP	5	akmxon	2020-01-01
PRO-SUP	5	vhlmshllke	2020-01-12
REQ-SUP	11	akmxon	2021-05-20
REQ-SUP	15	akmxon	2021-05-20
\.


--
-- Data for Name: status_batch_pengiriman; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.status_batch_pengiriman (kode, nama) FROM stdin;
PRO	Diproses
OTW	Dalam perjalanan
DLV	Selesai
HLG	Hilang
RET	Dikembalikan
\.


--
-- Data for Name: status_permohonan; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.status_permohonan (kode, nama) FROM stdin;
REQ	Diajukan
PRO	Diproses
WAI	Menunggu Pengadaan
FIN	Selesai
REJ	Ditolak
MAS	Selesai dengan Masalah
\.


--
-- Data for Name: status_pesanan; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.status_pesanan (kode, nama) FROM stdin;
REQ-SUP	Diajukan
REJ-SUP	Ditolak
PRO-SUP	Diproses
FIN-SUP	Selesai
MAS-SUP	Selesai dengan Masalah
\.


--
-- Data for Name: stok_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.stok_faskes (kode_faskes, kode_item_sumber_daya, jumlah) FROM stdin;
faskes-1	apd-1	100
faskes-1	ven-1	15
faskes-2	rap-2	500
faskes-2	ven-2	10
faskes-3	apd-1	150
faskes-3	apd-2	120
faskes-4	bed-1	50
faskes-4	ven-1	20
faskes-5	pcr-1	250
faskes-5	vak-1	200
\.


--
-- Data for Name: stok_warehouse_provinsi; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.stok_warehouse_provinsi (id_lokasi_warehouse, jumlah, kode_item_sumber_daya) FROM stdin;
l-1	1000	apd-1
l-2	1200	apd-2
l-3	100	ven-1
l-4	120	ven-2
l-5	200	bed-1
l-6	250	bed-2
l-7	2000	msk-1
l-8	2500	msk-1
l-9	1000	vak-1
l-10	1500	vak-2
\.


--
-- Data for Name: supplier; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.supplier (username, nama_organisasi) FROM stdin;
avictormarchington	PT Maju Mundur
akmxon	PT Haji Kudus
vhlmshllke	PT Selera Semua
dnstbbins	PT Mama Cinta
arglebargle	PT Sayang Mamae
adlaibr	PT Cinta Dia
\.


--
-- Data for Name: tipe_faskes; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.tipe_faskes (kode, nama_tipe) FROM stdin;
RUJ	Rumah Sakit rujukan
IDD	Instalasi darurat terpadu
KLI	Klinik
PUS	Puskesmas
\.


--
-- Data for Name: tipe_item; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.tipe_item (kode, nama) FROM stdin;
APD	Alat Pelindung Diri
VEN	Ventilator
BED	Tempat Tidur
MSK	Paket Masker
VAK	Paket Vaksin
PCR	PCR Test Kit
RAP	Rapid Test Kit
\.


--
-- Data for Name: transaksi_sumber_daya; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.transaksi_sumber_daya (nomor, tanggal, total_berat, total_item) FROM stdin;
1	2020-01-03	80	80
2	2020-01-04	100	100
3	2020-01-09	75	3
4	2020-01-11	110	55
5	2020-01-02	100	100
6	2020-01-07	20	10
7	2020-01-06	60	5
8	2020-01-01	150	150
9	2020-01-10	120	4
10	2020-01-08	100	100
11	2021-01-05	50	50
12	2021-02-05	50	50
13	2021-02-05	50	50
14	2021-02-05	\N	\N
15	2021-01-05	50	50
\.


--
-- Data for Name: warehouse_provinsi; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.warehouse_provinsi (id_lokasi) FROM stdin;
l-1
l-2
l-3
l-4
l-5
l-6
l-7
l-8
l-9
l-10
\.


--
-- Data for Name: warehouse_supplier; Type: TABLE DATA; Schema: sidia; Owner: db202f15
--

COPY sidia.warehouse_supplier (id_lokasi, username_supplier) FROM stdin;
l-2	avictormarchington
l-4	akmxon
l-6	vhlmshllke
l-8	dnstbbins
l-10	arglebargle
\.


--
-- Name: admin_satgas admin_satgas_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.admin_satgas
    ADD CONSTRAINT admin_satgas_pkey PRIMARY KEY (username);


--
-- Name: batch_pengiriman batch_pengiriman_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_pkey PRIMARY KEY (kode);


--
-- Name: daftar_item daftar_item_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.daftar_item
    ADD CONSTRAINT daftar_item_pkey PRIMARY KEY (no_transaksi_sumber_daya, no_urut);


--
-- Name: faskes faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_pkey PRIMARY KEY (kode_faskes_nasional);


--
-- Name: item_sumber_daya item_sumber_daya_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.item_sumber_daya
    ADD CONSTRAINT item_sumber_daya_pkey PRIMARY KEY (kode);


--
-- Name: kendaraan kendaraan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.kendaraan
    ADD CONSTRAINT kendaraan_pkey PRIMARY KEY (nomor);


--
-- Name: lokasi lokasi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.lokasi
    ADD CONSTRAINT lokasi_pkey PRIMARY KEY (id);


--
-- Name: manifes_item manifes_item_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.manifes_item
    ADD CONSTRAINT manifes_item_pkey PRIMARY KEY (kode_batch_pengiriman, no_urut);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (username);


--
-- Name: permohonan_sumber_daya_faskes permohonan_sumber_daya_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.permohonan_sumber_daya_faskes
    ADD CONSTRAINT permohonan_sumber_daya_faskes_pkey PRIMARY KEY (no_transaksi_sumber_daya);


--
-- Name: pesanan_sumber_daya pesanan_sumber_daya_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.pesanan_sumber_daya
    ADD CONSTRAINT pesanan_sumber_daya_pkey PRIMARY KEY (nomor_pesanan);


--
-- Name: petugas_distribusi petugas_distribusi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.petugas_distribusi
    ADD CONSTRAINT petugas_distribusi_pkey PRIMARY KEY (username);


--
-- Name: petugas_faskes petugas_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.petugas_faskes
    ADD CONSTRAINT petugas_faskes_pkey PRIMARY KEY (username);


--
-- Name: riwayat_status_pengiriman riwayat_status_pengiriman_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_pengiriman
    ADD CONSTRAINT riwayat_status_pengiriman_pkey PRIMARY KEY (kode_status_batch_pengiriman, kode_batch);


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_pkey PRIMARY KEY (kode_status_permohonan, nomor_permohonan);


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_pkey PRIMARY KEY (kode_status_pesanan, no_pesanan);


--
-- Name: status_batch_pengiriman status_batch_pengiriman_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.status_batch_pengiriman
    ADD CONSTRAINT status_batch_pengiriman_pkey PRIMARY KEY (kode);


--
-- Name: status_permohonan status_permohonan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.status_permohonan
    ADD CONSTRAINT status_permohonan_pkey PRIMARY KEY (kode);


--
-- Name: status_pesanan status_pesanan_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.status_pesanan
    ADD CONSTRAINT status_pesanan_pkey PRIMARY KEY (kode);


--
-- Name: stok_faskes stok_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.stok_faskes
    ADD CONSTRAINT stok_faskes_pkey PRIMARY KEY (kode_faskes, kode_item_sumber_daya);


--
-- Name: stok_warehouse_provinsi stok_warehouse_provinsi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.stok_warehouse_provinsi
    ADD CONSTRAINT stok_warehouse_provinsi_pkey PRIMARY KEY (id_lokasi_warehouse, kode_item_sumber_daya);


--
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (username);


--
-- Name: tipe_faskes tipe_faskes_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.tipe_faskes
    ADD CONSTRAINT tipe_faskes_pkey PRIMARY KEY (kode);


--
-- Name: tipe_item tipe_item_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.tipe_item
    ADD CONSTRAINT tipe_item_pkey PRIMARY KEY (kode);


--
-- Name: transaksi_sumber_daya transaksi_sumber_daya_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.transaksi_sumber_daya
    ADD CONSTRAINT transaksi_sumber_daya_pkey PRIMARY KEY (nomor);


--
-- Name: warehouse_provinsi warehouse_provinsi_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.warehouse_provinsi
    ADD CONSTRAINT warehouse_provinsi_pkey PRIMARY KEY (id_lokasi);


--
-- Name: warehouse_supplier warehouse_supplier_pkey; Type: CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.warehouse_supplier
    ADD CONSTRAINT warehouse_supplier_pkey PRIMARY KEY (id_lokasi);


--
-- Name: batch_pengiriman batch_pengiriman_asal_check; Type: TRIGGER; Schema: sidia; Owner: db202f15
--

CREATE TRIGGER batch_pengiriman_asal_check BEFORE INSERT ON sidia.batch_pengiriman FOR EACH ROW EXECUTE PROCEDURE sidia.batch_pengiriman_asal_check();


--
-- Name: batch_pengiriman batch_pengiriman_tujuan_check; Type: TRIGGER; Schema: sidia; Owner: db202f15
--

CREATE TRIGGER batch_pengiriman_tujuan_check BEFORE INSERT ON sidia.batch_pengiriman FOR EACH ROW EXECUTE PROCEDURE sidia.batch_pengiriman_tujuan_check();


--
-- Name: permohonan_sumber_daya_faskes check_stok_permohonan; Type: TRIGGER; Schema: sidia; Owner: db202f15
--

CREATE TRIGGER check_stok_permohonan AFTER INSERT ON sidia.permohonan_sumber_daya_faskes FOR EACH ROW EXECUTE PROCEDURE sidia.check_stok_permohonan();


--
-- Name: pengguna username_pengguna_check; Type: TRIGGER; Schema: sidia; Owner: db202f15
--

CREATE TRIGGER username_pengguna_check BEFORE INSERT OR UPDATE OF username ON sidia.pengguna FOR EACH ROW EXECUTE PROCEDURE sidia.username_pengguna_check();


--
-- Name: pesanan_sumber_daya validate_pesanan; Type: TRIGGER; Schema: sidia; Owner: db202f15
--

CREATE TRIGGER validate_pesanan AFTER INSERT ON sidia.pesanan_sumber_daya FOR EACH ROW EXECUTE PROCEDURE sidia.validate_pesanan_sumber_daya();


--
-- Name: admin_satgas admin_satgas_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.admin_satgas
    ADD CONSTRAINT admin_satgas_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_id_lokasi_asal_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_id_lokasi_asal_fkey FOREIGN KEY (id_lokasi_asal) REFERENCES sidia.lokasi(id);


--
-- Name: batch_pengiriman batch_pengiriman_id_lokasi_tujuan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_id_lokasi_tujuan_fkey FOREIGN KEY (id_lokasi_tujuan) REFERENCES sidia.lokasi(id);


--
-- Name: batch_pengiriman batch_pengiriman_no_kendaraan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_no_kendaraan_fkey FOREIGN KEY (no_kendaraan) REFERENCES sidia.kendaraan(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: batch_pengiriman batch_pengiriman_nomor_transaksi_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_nomor_transaksi_sumber_daya_fkey FOREIGN KEY (nomor_transaksi_sumber_daya) REFERENCES sidia.transaksi_sumber_daya(nomor);


--
-- Name: batch_pengiriman batch_pengiriman_username_petugas_distribusi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_username_petugas_distribusi_fkey FOREIGN KEY (username_petugas_distribusi) REFERENCES sidia.petugas_distribusi(username);


--
-- Name: batch_pengiriman batch_pengiriman_username_satgas_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.batch_pengiriman
    ADD CONSTRAINT batch_pengiriman_username_satgas_fkey FOREIGN KEY (username_satgas) REFERENCES sidia.admin_satgas(username);


--
-- Name: daftar_item daftar_item_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.daftar_item
    ADD CONSTRAINT daftar_item_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: daftar_item daftar_item_no_transaksi_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.daftar_item
    ADD CONSTRAINT daftar_item_no_transaksi_sumber_daya_fkey FOREIGN KEY (no_transaksi_sumber_daya) REFERENCES sidia.transaksi_sumber_daya(nomor);


--
-- Name: faskes faskes_id_lokasi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_id_lokasi_fkey FOREIGN KEY (id_lokasi) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: faskes faskes_kode_tipe_faskes_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_kode_tipe_faskes_fkey FOREIGN KEY (kode_tipe_faskes) REFERENCES sidia.tipe_faskes(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: faskes faskes_username_petugas_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.faskes
    ADD CONSTRAINT faskes_username_petugas_fkey FOREIGN KEY (username_petugas) REFERENCES sidia.petugas_faskes(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: item_sumber_daya item_sumber_daya_kode_tipe_item_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.item_sumber_daya
    ADD CONSTRAINT item_sumber_daya_kode_tipe_item_fkey FOREIGN KEY (kode_tipe_item) REFERENCES sidia.tipe_item(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: item_sumber_daya item_sumber_daya_username_supplier_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.item_sumber_daya
    ADD CONSTRAINT item_sumber_daya_username_supplier_fkey FOREIGN KEY (username_supplier) REFERENCES sidia.supplier(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: manifes_item manifes_item_kode_batch_pengiriman_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.manifes_item
    ADD CONSTRAINT manifes_item_kode_batch_pengiriman_fkey FOREIGN KEY (kode_batch_pengiriman) REFERENCES sidia.batch_pengiriman(kode);


--
-- Name: manifes_item manifes_item_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.manifes_item
    ADD CONSTRAINT manifes_item_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permohonan_sumber_daya_faskes permohonan_sumber_daya_faskes_no_transaksi_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.permohonan_sumber_daya_faskes
    ADD CONSTRAINT permohonan_sumber_daya_faskes_no_transaksi_sumber_daya_fkey FOREIGN KEY (no_transaksi_sumber_daya) REFERENCES sidia.transaksi_sumber_daya(nomor);


--
-- Name: permohonan_sumber_daya_faskes permohonan_sumber_daya_faskes_username_petugas_faskes_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.permohonan_sumber_daya_faskes
    ADD CONSTRAINT permohonan_sumber_daya_faskes_username_petugas_faskes_fkey FOREIGN KEY (username_petugas_faskes) REFERENCES sidia.petugas_faskes(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pesanan_sumber_daya pesanan_sumber_daya_nomor_pesanan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.pesanan_sumber_daya
    ADD CONSTRAINT pesanan_sumber_daya_nomor_pesanan_fkey FOREIGN KEY (nomor_pesanan) REFERENCES sidia.transaksi_sumber_daya(nomor);


--
-- Name: pesanan_sumber_daya pesanan_sumber_daya_username_admin_satgas_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.pesanan_sumber_daya
    ADD CONSTRAINT pesanan_sumber_daya_username_admin_satgas_fkey FOREIGN KEY (username_admin_satgas) REFERENCES sidia.admin_satgas(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: petugas_distribusi petugas_distribusi_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.petugas_distribusi
    ADD CONSTRAINT petugas_distribusi_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: petugas_faskes petugas_faskes_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.petugas_faskes
    ADD CONSTRAINT petugas_faskes_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pengiriman riwayat_status_pengiriman_kode_batch_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_pengiriman
    ADD CONSTRAINT riwayat_status_pengiriman_kode_batch_fkey FOREIGN KEY (kode_batch) REFERENCES sidia.batch_pengiriman(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pengiriman riwayat_status_pengiriman_kode_status_batch_pengiriman_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_pengiriman
    ADD CONSTRAINT riwayat_status_pengiriman_kode_status_batch_pengiriman_fkey FOREIGN KEY (kode_status_batch_pengiriman) REFERENCES sidia.status_batch_pengiriman(kode);


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_kode_status_permohonan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_kode_status_permohonan_fkey FOREIGN KEY (kode_status_permohonan) REFERENCES sidia.status_permohonan(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_nomor_permohonan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_nomor_permohonan_fkey FOREIGN KEY (nomor_permohonan) REFERENCES sidia.permohonan_sumber_daya_faskes(no_transaksi_sumber_daya) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_permohonan riwayat_status_permohonan_username_admin_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_permohonan
    ADD CONSTRAINT riwayat_status_permohonan_username_admin_fkey FOREIGN KEY (username_admin) REFERENCES sidia.admin_satgas(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_kode_status_pesanan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_kode_status_pesanan_fkey FOREIGN KEY (kode_status_pesanan) REFERENCES sidia.status_pesanan(kode);


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_no_pesanan_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_no_pesanan_fkey FOREIGN KEY (no_pesanan) REFERENCES sidia.pesanan_sumber_daya(nomor_pesanan);


--
-- Name: riwayat_status_pesanan riwayat_status_pesanan_username_supplier_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.riwayat_status_pesanan
    ADD CONSTRAINT riwayat_status_pesanan_username_supplier_fkey FOREIGN KEY (username_supplier) REFERENCES sidia.supplier(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_faskes stok_faskes_kode_faskes_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.stok_faskes
    ADD CONSTRAINT stok_faskes_kode_faskes_fkey FOREIGN KEY (kode_faskes) REFERENCES sidia.faskes(kode_faskes_nasional) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_faskes stok_faskes_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.stok_faskes
    ADD CONSTRAINT stok_faskes_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_warehouse_provinsi stok_warehouse_provinsi_id_lokasi_warehouse_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.stok_warehouse_provinsi
    ADD CONSTRAINT stok_warehouse_provinsi_id_lokasi_warehouse_fkey FOREIGN KEY (id_lokasi_warehouse) REFERENCES sidia.warehouse_provinsi(id_lokasi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: stok_warehouse_provinsi stok_warehouse_provinsi_kode_item_sumber_daya_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.stok_warehouse_provinsi
    ADD CONSTRAINT stok_warehouse_provinsi_kode_item_sumber_daya_fkey FOREIGN KEY (kode_item_sumber_daya) REFERENCES sidia.item_sumber_daya(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: supplier supplier_username_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.supplier
    ADD CONSTRAINT supplier_username_fkey FOREIGN KEY (username) REFERENCES sidia.pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: warehouse_provinsi warehouse_provinsi_id_lokasi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.warehouse_provinsi
    ADD CONSTRAINT warehouse_provinsi_id_lokasi_fkey FOREIGN KEY (id_lokasi) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: warehouse_supplier warehouse_supplier_id_lokasi_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.warehouse_supplier
    ADD CONSTRAINT warehouse_supplier_id_lokasi_fkey FOREIGN KEY (id_lokasi) REFERENCES sidia.lokasi(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: warehouse_supplier warehouse_supplier_username_supplier_fkey; Type: FK CONSTRAINT; Schema: sidia; Owner: db202f15
--

ALTER TABLE ONLY sidia.warehouse_supplier
    ADD CONSTRAINT warehouse_supplier_username_supplier_fkey FOREIGN KEY (username_supplier) REFERENCES sidia.supplier(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--