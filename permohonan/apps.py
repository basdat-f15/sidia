from django.apps import AppConfig


class PermohonanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'permohonan'
