import django.contrib.auth
from django.urls import path, include
from . import views

app_name = 'permohonan'

urlpatterns = [
    path('', views.permohonan, name='permohonan'),
]
