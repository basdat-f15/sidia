from django.db import connection
from django.shortcuts import render, redirect

from .utils import dictfetchall

def permohonan(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute('SELECT * FROM PESANAN_SUMBER_DAYA')
    context = {
        'pesanan_sumber_daya': dictfetchall(cursor)
    }

    return render(request, 'permohonan/permohonan.html', context)