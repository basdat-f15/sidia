from django.shortcuts import redirect
from functools import wraps

def is_user_logged_in():
    def decorator(func):
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            if request.session.get('user_role'):
                return redirect('dashboard:dashboard')
            else:
                return func(request, *args, **kwargs)
        return wrapper
    return decorator

def is_user_role_correct(role):
    def decorator(func):
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            if request.session.get('user_role') not in role:
                return redirect('dashboard:dashboard')
            else:
                return func(request, *args, **kwargs)
        return wrapper
    return decorator