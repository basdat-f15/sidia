from django.db import connection

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]

    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def assign_session(request, username):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    user_role = (
        query_by_username(username, "ADMIN_SATGAS") or
        query_by_username(username, "PETUGAS_FASKES") or
        query_by_username(username, "SUPPLIER") or
        query_by_username(username, "PETUGAS_DISTRIBUSI")
    )

    request.session.modified = True
    request.session['user_role'] = user_role
    request.session['username'] = username

def query_by_username(username, table):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute(f'''
        SELECT username
        FROM {table}
        WHERE username = '{username}'
    ;''')

    return table if dictfetchall(cursor) != [] else ""

def register_user(form):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute(f'''
        INSERT INTO PENGGUNA VALUES (
            '{form['username']}',
            '{form['password']}',
            '{form['nama']}',
            '{form['kelurahan']}',
            '{form['kecamatan']}',
            '{form['kota'].upper()}',
            '{form['provinsi'].upper()}',
            '{form['nomor-telepon']}'
        );
    ''')

    register_role(form)


def register_role(form):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    register_as = form['mendaftar-sebagai']

    if (register_as == "ADMIN_SATGAS" or
        register_as == "PETUGAS_FASKES"):
        cursor.execute(f'''
            INSERT INTO {register_as} VALUES (
                '{form['username']}'
            );
        ''')
    elif (register_as == "SUPPLIER"):\
        cursor.execute(f'''
            INSERT INTO SUPPLIER VALUES (
                '{form['username']}',
                '{form['nama-organisasi']}'
            );
        ''')
    elif (register_as == "PETUGAS_DISTRIBUSI"):
        cursor.execute(f'''
            INSERT INTO PETUGAS_DISTRIBUSI VALUES (
                '{form['username']}',
                '{form['nomor-sim']}'
            );
        ''')