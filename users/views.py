from django.db import connection
from django.contrib import messages
from django.shortcuts import render, redirect

from .decorators import is_user_logged_in, is_user_role_correct
from .utils import (dictfetchall, assign_session,
                query_by_username, register_user)

@is_user_logged_in()
def login(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    if request.method == 'POST':
        form = request.POST
        username = form['username']
        password = form['password']

        cursor.execute(f'''
            SELECT
                username,
                password
            FROM PENGGUNA
            WHERE
                username = '{username}' AND
                password = '{password}'
        ;''')

        if (dictfetchall(cursor) == []):
            messages.error(request, "Username atau Password yang anda masukkan salah.")
            return redirect('users:login')

        assign_session(request, username)
        return redirect('dashboard:dashboard')
    return render(request, 'users/login.html')

@is_user_logged_in()
def register(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    if request.method == 'POST':
        form = request.POST
        print(form)
        username = form['username']
        register_as = form['mendaftar-sebagai']

        cursor.execute(f'''
            SELECT
                username
            FROM PENGGUNA
            WHERE
                username = '{username}'
        ;''')

        if (dictfetchall(cursor) != []):
            user_role = (
                query_by_username(username, "ADMIN_SATGAS") or
                query_by_username(username, "PETUGAS_FASKES") or
                query_by_username(username, "SUPPLIER") or
                query_by_username(username, "PETUGAS_DISTRIBUSI")
            )

            user_role = user_role.split("_")
            user_role = " ".join(user_role).lower().title()

            messages.error(request, f"Username {username} sudah terdaftar dengan peran {user_role}")
            return redirect('users:register')

        register_user(form)
        return redirect('users:login')
    return render(request, 'users/register.html')

def logout(request):
    try:
        if request.session['username'] != None:
            del request.session['username']
            del request.session['user_role']
            return redirect('dashboard:landing')
        else:
            return redirect('users:login')
    except:
        return redirect('users:login')