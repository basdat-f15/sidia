from django.db import connection
from django.shortcuts import render, redirect

from .utils import dictfetchall
from users.decorators import is_user_logged_in, is_user_role_correct

def landing(request):
    return render(request, 'dashboard/landing.html')

def dashboard(request):
    user_role = request.session.get('user_role')

    if user_role == "ADMIN_SATGAS":
        return redirect('dashboard:dashboard-admin-satgas')
    elif user_role == "PETUGAS_FASKES":
        return redirect('dashboard:dashboard-petugas-faskes')
    elif user_role == "SUPPLIER":
        return redirect('dashboard:dashboard-supplier')
    elif user_role == "PETUGAS_DISTRIBUSI":
        return redirect('dashboard:dashboard-petugas-distribusi')

    return redirect('users:login')

@is_user_role_correct(["ADMIN_SATGAS"])
def dashboard_admin_satgas(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    username = request.session.get('username')

    cursor.execute(f'''
        SELECT DISTINCT *
        FROM PENGGUNA
        WHERE username = '{username}'
    ;''')

    profile_data = dictfetchall(cursor)[0]

    context = {}
    context['profile_data'] = profile_data
    context['role_data'] = "Admin Satgas"

    return render(request, 'dashboard/admin-satgas.html', context)

@is_user_role_correct(["PETUGAS_FASKES"])
def dashboard_petugas_faskes(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    username = request.session.get('username')

    cursor.execute(f'''
        SELECT DISTINCT *
        FROM PENGGUNA
        WHERE username = '{username}'
    ;''')

    profile_data = dictfetchall(cursor)[0]

    context = {}
    context['profile_data'] = profile_data
    context['role_data'] = "Petugas Faskes"

    return render(request, 'dashboard/petugas-faskes.html', context)

@is_user_role_correct(["SUPPLIER"])
def dashboard_supplier(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    username = request.session.get('username')

    cursor.execute(f'''
        SELECT DISTINCT *
        FROM
            PENGGUNA p,
            SUPPLIER s
        WHERE
            p.username = '{username}' AND
            p.username = s.username
    ;''')

    profile_data = dictfetchall(cursor)[0]

    context = {}
    context['profile_data'] = profile_data
    context['role_data'] = "Supplier"

    return render(request, 'dashboard/supplier.html', context)

@is_user_role_correct(["PETUGAS_DISTRIBUSI"])
def dashboard_petugas_distribusi(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    username = request.session.get('username')

    cursor.execute(f'''
        SELECT DISTINCT *
        FROM
            PENGGUNA p,
            PETUGAS_DISTRIBUSI pd
        WHERE
            p.username = '{username}' AND
            p.username = pd.username
    ;''')

    profile_data = dictfetchall(cursor)[0]

    context = {}
    context['profile_data'] = profile_data
    context['role_data'] = "Petugas Distribusi"

    return render(request, 'dashboard/petugas-distribusi.html', context)