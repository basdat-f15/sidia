import django.contrib.auth
from django.urls import path, include
from . import views

app_name = 'dashboard'

urlpatterns = [
    path('', views.landing, name='landing'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('dashboard/admin/', views.dashboard_admin_satgas, name='dashboard-admin-satgas'),
    path('dashboard/faskes/', views.dashboard_petugas_faskes, name='dashboard-petugas-faskes'),
    path('dashboard/supplier/', views.dashboard_supplier, name='dashboard-supplier'),
    path('dashboard/distribusi/', views.dashboard_petugas_distribusi, name='dashboard-petugas-distribusi'),
]