from django.apps import AppConfig


class KendaraanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kendaraan'
