import django.contrib.auth
from django.urls import path, include
from . import views

app_name = 'kendaraan'

urlpatterns = [
    path('', views.kendaraan, name='kendaraan'),
]
