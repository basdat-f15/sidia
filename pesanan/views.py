from django.db import connection
from django.shortcuts import render, redirect

from .utils import (dictfetchall, pesanan_query_admin,
                pesanan_query_supplier, stok_faskes_query,
                stok_faskes_create_query, stok_faskes_create_insert,
                stok_faskes_update_query, stok_faskes_update_update,
                stok_faskes_delete_delete, riwayat_create_ops,
                riwayat_status_query, pesanan_delete_delete)
from users.decorators import is_user_role_correct

'''
Trigger no. 3
6. CRUD Pesanan Sumber Daya
    - Admin (CRUD)
    - Supplier (R)
7. CR Riwayat Status Pesanan Sumber Daya
    - Admin (R)
    - Supplier (CR)
8. CRUD Stok Fasilitas Kesehatan
    - Admin (CRUD)
    - Petugas Faskes (R)
'''

@is_user_role_correct(["ADMIN_SATGAS", "SUPPLIER"])
def pesanan(request):
    user_role = request.session.get('user_role')
    username = request.session.get('username')

    order_data = []
    if (user_role == "ADMIN_SATGAS"):
        order_data = pesanan_query_admin(user_role, username)
    elif (user_role == "SUPPLIER"):
        order_data = pesanan_query_supplier(user_role, username)

    user_role = user_role.split("_")
    user_role = " ".join(user_role).lower().title()

    context = {}
    context['order_data'] = order_data if order_data != [] else []
    context['user_role'] = user_role
    context['username'] = username

    return render(request, 'pesanan/pesanan.html', context)

@is_user_role_correct(["ADMIN_SATGAS"])
def pesanan_create(request):
    user_role = request.session.get('user_role')
    username = request.session.get('username')

    return render(request, 'pesanan/pesanan-create.html')

@is_user_role_correct(["ADMIN_SATGAS"])
def pesanan_delete(request, no_transaksi):
    pesanan_delete(no_transaksi)
    redirect('pesanan:pesanan')

@is_user_role_correct(["ADMIN_SATGAS", "PETUGAS_FASKES"])
def stok_faskes(request):
    user_role = request.session.get('user_role')
    username = request.session.get('username')

    stock_data = stok_faskes_query(user_role, username)

    user_role = user_role.split("_")
    user_role = " ".join(user_role).lower().title()

    context = {}
    context['stock_data'] = stock_data if stock_data != [] else []
    context['user_role'] = user_role
    context['username'] = username

    return render(request, 'pesanan/stok-faskes.html', context)

@is_user_role_correct(["ADMIN_SATGAS"])
def stok_faskes_create(request):
    [faskes_data, item_data] = stok_faskes_create_query()

    if request.method == 'POST':
        form = request.POST
        stok_faskes_create_insert(form)
        return redirect('pesanan:stok-faskes')

    context = {}
    context['faskes_data'] = faskes_data
    context['item_data'] = item_data

    return render(request, 'pesanan/stok-faskes-create.html', context)

@is_user_role_correct(["ADMIN_SATGAS"])
def stok_faskes_update(request, kode_faskes, kode_item):
    [faskes_data, item_data] = stok_faskes_update_query(kode_faskes, kode_item)

    if request.method == 'POST':
        form = request.POST
        stok_faskes_update_update(form)
        return redirect('pesanan:stok-faskes')

    context = {}
    context['faskes_data'] = faskes_data[0]
    context['item_data'] = item_data[0]

    return render(request, 'pesanan/stok-faskes-update.html', context)

@is_user_role_correct(["ADMIN_SATGAS"])
def stok_faskes_delete(request, kode_faskes, kode_item):
    stok_faskes_delete_delete(kode_faskes, kode_item)
    return redirect('pesanan:stok-faskes')

@is_user_role_correct(["SUPPLIER"])
def riwayat_process(request, no_transaksi):
    username = request.session.get('username')
    riwayat_create_ops(username, no_transaksi, "PRO-SUP")
    return redirect('pesanan:pesanan')

@is_user_role_correct(["SUPPLIER"])
def riwayat_reject(request, no_transaksi):
    username = request.session.get('username')
    riwayat_create_ops(username, no_transaksi, "REJ-SUP")
    return redirect('pesanan:pesanan')

@is_user_role_correct(["SUPPLIER"])
def riwayat_problem(request, no_transaksi):
    username = request.session.get('username')
    riwayat_create_ops(username, no_transaksi, "MAS-SUP")
    return redirect('pesanan:pesanan')

@is_user_role_correct(["SUPPLIER"])
def riwayat_finish(request, no_transaksi):
    username = request.session.get('username')
    riwayat_create_ops(username, no_transaksi, "FIN-SUP")
    return redirect('pesanan:pesanan')

@is_user_role_correct(["ADMIN_SATGAS", "SUPPLIER"])
def riwayat_status(request, no_transaksi):
    user_role = request.session.get('user_role')
    username = request.session.get('username')

    riwayat_data = riwayat_status_query(no_transaksi)

    count = 1
    for riwayat in riwayat_data:
        riwayat['no'] = count
        count += 1

    user_role = user_role.split("_")
    user_role = " ".join(user_role).lower().title()

    context = {}
    context['riwayat_data'] = riwayat_data
    context['no_transaksi'] = no_transaksi
    context['user_role'] = user_role
    context['username'] = username

    return render(request, 'pesanan/riwayat.html', context)