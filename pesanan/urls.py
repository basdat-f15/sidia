import django.contrib.auth
from django.urls import path, include
from . import views

app_name = 'pesanan'

urlpatterns = [
    path('pesanan/', views.pesanan, name='pesanan'),
    path('pesanan/create/', views.pesanan_create, name='pesanan-create'),
    path('riwayat/process/<str:no_transaksi>/', views.riwayat_process, name='riwayat-process'),
    path('riwayat/reject/<str:no_transaksi>/', views.riwayat_reject, name='riwayat-reject'),
    path('riwayat/problem/<str:no_transaksi>/', views.riwayat_problem, name='riwayat-problem'),
    path('riwayat/finish/<str:no_transaksi>/', views.riwayat_finish, name='riwayat-finish'),
    path('riwayat/status/<str:no_transaksi>/', views.riwayat_status, name='riwayat-status'),
    path('stok-faskes/', views.stok_faskes, name='stok-faskes'),
    path('stok-faskes/create/', views.stok_faskes_create, name='stok-faskes-create'),
    path('stok-faskes/update/<str:kode_faskes>/<str:kode_item>', views.stok_faskes_update, name='stok-faskes-update'),
    path('stok-faskes/delete/<str:kode_faskes>/<str:kode_item>', views.stok_faskes_delete, name='stok-faskes-delete'),
]
