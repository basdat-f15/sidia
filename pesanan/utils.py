from django.db import connection

from datetime import datetime

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]

    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def pesanan_query_admin(user_role, username):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute(f'''
        SELECT
            tsd.nomor,
            psd.username_admin_satgas,
            rsp.tanggal,
            tsd.total_berat,
            tsd.total_item,
            psd.total_harga,
            sp.kode
        FROM
            TRANSAKSI_SUMBER_DAYA tsd,
            PESANAN_SUMBER_DAYA psd,
            RIWAYAT_STATUS_PESANAN rsp,
            STATUS_PESANAN sp
        WHERE
            psd.username_admin_satgas = '{username}' AND
            tsd.nomor = psd.nomor_pesanan AND
            psd.nomor_pesanan = rsp.no_pesanan AND
            rsp.kode_status_pesanan = sp.kode
        ORDER BY
            rsp.tanggal DESC
    ;''')

    return dictfetchall(cursor)

def pesanan_query_supplier(user_role, username):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute(f'''
        SELECT
            tsd.nomor,
            psd.username_admin_satgas,
            rsp.tanggal,
            tsd.total_berat,
            tsd.total_item,
            psd.total_harga,
            sp.kode
        FROM
            TRANSAKSI_SUMBER_DAYA tsd,
            PESANAN_SUMBER_DAYA psd,
            RIWAYAT_STATUS_PESANAN rsp,
            STATUS_PESANAN sp
        WHERE
            tsd.nomor = psd.nomor_pesanan AND
            psd.nomor_pesanan = rsp.no_pesanan AND
            rsp.kode_status_pesanan = sp.kode
        ORDER BY
            rsp.tanggal DESC

    ;''')

    return dictfetchall(cursor)

def stok_faskes_query(user_role, username):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute(f'''
        SELECT
            sf.kode_faskes,
            tf.nama_tipe,
            isd.kode as kode_item,
            isd.nama,
            sf.jumlah as jumlah_item
        FROM
            TIPE_FASKES tf,
            FASKES f,
            STOK_FASKES sf,
            ITEM_SUMBER_DAYA isd
        WHERE
            tf.kode = f.kode_tipe_faskes AND
            f.kode_faskes_nasional = sf.kode_faskes AND
            sf.kode_item_sumber_daya = isd.kode
    ''')

    return dictfetchall(cursor)

def stok_faskes_create_query():
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute('''
        SELECT
            kode_faskes_nasional,
            nama_tipe
        FROM
            FASKES f,
            TIPE_FASKES tf
        WHERE
            f.kode_tipe_faskes = tf.kode
    ;''')
    faskes_data = dictfetchall(cursor)

    cursor.execute('''
        SELECT
            kode,
            nama
        FROM
            ITEM_SUMBER_DAYA
    ;''')
    item_data = dictfetchall(cursor)

    return [faskes_data, item_data]

def stok_faskes_create_insert(form):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    kode_faskes = form['kode-faskes']
    kode_item = form['kode-item-sumber-daya']
    jumlah = form['jumlah-item']

    cursor.execute(f'''
        INSERT INTO STOK_FASKES VALUES (
            '{kode_faskes}',
            '{kode_item}',
            {int(jumlah)}
        );
    ''')

def stok_faskes_update_query(kode_faskes, kode_item):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute(f'''
        SELECT DISTINCT
            kode_faskes_nasional,
            nama_tipe
        FROM
            FASKES f,
            TIPE_FASKES tf
        WHERE
            f.kode_tipe_faskes = tf.kode AND
            f.kode_faskes_nasional = '{kode_faskes}'
    ;''')
    faskes_data = dictfetchall(cursor)

    cursor.execute(f'''
        SELECT DISTINCT
            kode,
            nama
        FROM
            ITEM_SUMBER_DAYA
        WHERE
            kode = '{kode_item}'
    ;''')
    item_data = dictfetchall(cursor)

    return [faskes_data, item_data]

def stok_faskes_update_update(form):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    kode_faskes = form['kode-faskes']
    kode_item = form['kode-item-sumber-daya']
    jumlah = form['jumlah-item']

    cursor.execute(f'''
        UPDATE
            STOK_FASKES
        SET
            jumlah = {int(jumlah)}
        WHERE
            kode_faskes = '{kode_faskes}' AND
            kode_item_sumber_daya = '{kode_item}'
    ;''')

def stok_faskes_delete_delete(kode_faskes, kode_item):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute(f'''
        DELETE
        FROM
            STOK_FASKES
        WHERE
            kode_faskes = '{kode_faskes}' AND
            kode_item_sumber_daya = '{kode_item}'
    ;''')

def riwayat_create_ops(username, no_transaksi, kode_status):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    date = datetime.today().strftime('%Y-%m-%d')

    cursor.execute(f'''
        INSERT INTO RIWAYAT_STATUS_PESANAN VALUES (
            '{kode_status}',
            {no_transaksi},
            '{username}',
            '{date}'
        );
    ''')

def riwayat_status_query(no_transaksi):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute(f'''
        SELECT
            no_pesanan,
            kode_status_pesanan,
            nama,
            username_supplier,
            tanggal
        FROM
            RIWAYAT_STATUS_PESANAN,
            STATUS_PESANAN
        WHERE
            kode_status_pesanan = kode
        ORDER BY
            tanggal DESC
    ;''')

    return dictfetchall(cursor)

def pesanan_delete_delete(no_transaksi):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA')

    cursor.execute(f'''
        DELETE
        FROM
            PESANAN_SUMBER_DAYA
        WHERE
            no_pesanan = {no_transaksi}
    ''')