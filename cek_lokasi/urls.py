import django.contrib.auth
from django.urls import path, include
from . import views

app_name = 'cek_lokasi'

urlpatterns = [
    # path('', views.cek_lokasi, name='cek_lokasi'),
    path('tambah-faskes/', views.tambah_faskes, name='tambah_faskes'),
    path('tambah-warehouse-satgas/', views.tambah_warehouse_satgas, name='tambah_warehouse_satgas'),
    path('tambah-batch-pengiriman/', views.tambah_batch_pengiriman, name='tambah_batch_pengiriman'),
    path('response-lokasi/', views.perantara, name='perantara'),
    path('response-id/', views.perantara_id, name='perantara_id'),
    path('response-kode/', views.perantara_kode, name='perantara_kode'),
    path('daftar-faskes/', views.insert_to_faskes_table, name='insert_to_faskes_table'),
    path('daftar-warehouse-satgas/', views.insert_to_ws_table, name='insert_to_ws_table'),
    path('faskes/delete/<str:kode_faskes>', views.delete_faskes, name='delete_faskes'),
    path('ws/delete/<str:provinsi>/<str:kabupaten>/<str:kecamatan>/<str:kelurahan>/<str:jalan_no>', views.delete_ws, name='delete_ws'),

]
