from django.shortcuts import redirect, render
from django.db import connection
from .utils import dictfetchall, tambah_faskes, tambah_ws, faskes_delete, ws_delete, get_id_lokasi

from django.http import JsonResponse, request
from django.http import HttpResponse
import json
from users.decorators import is_user_role_correct
# from cek_lokasi.utils import faskes_delete

def tambah_batch_pengiriman(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')
    cursor.execute(f'''
        SELECT 
            pd.username
        FROM 
            PETUGAS_DISTRIBUSI pd
        WHERE
            pd.username not in (
                SELECT 
                    username_petugas_distribusi
                FROM 
                    BATCH_PENGIRIMAN bp,
                    STATUS_BATCH_PENGIRIMAN sbp
                WHERE
                    sbp.kode = 'PRO' OR sbp.kode = 'OTW'
            )
    ;''')

    petugas_satgas = {
        'petugas_satgas': dictfetchall(cursor)
    }

    return render(request, 'cek_lokasi/add_warehouse_satgas.html', petugas_satgas)


@is_user_role_correct(["ADMIN_SATGAS"])
def tambah_warehouse_satgas(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')
    cursor.execute('SELECT distinct provinsi FROM LOKASI WHERE id not in (SELECT id_lokasi FROM WAREHOUSE_PROVINSI);')

    context = {
        'provinsi_tersedia': dictfetchall(cursor)
    }

    provinsi_tersedia = set()
    for i in context['provinsi_tersedia']:
        for j in i:

            provinsi_tersedia.add(i['provinsi'].lower())

    provinsi_tersedia = sorted(provinsi_tersedia)

    # print(provinsi_tersedia)
    # print(len(provinsi_tersedia))

    provinsi_sisa = {
        'provinsi' : provinsi_tersedia
    }

    return render(request, 'cek_lokasi/add_warehouse_satgas.html', provinsi_sisa)

@is_user_role_correct(["ADMIN_SATGAS"])
def tambah_faskes(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')

    cursor.execute('SELECT Id FROM LOKASI;')
    
    context = {
        'id': dictfetchall(cursor)
    }

    temp_id = set()

    id_lokasi = {}
    for i in context['id']:
        for j in i:
            temp_id.add(i['id'])
        
    temp_id = sorted(temp_id)

    id_lokasi = {
        'id' : temp_id
    }

    cursor.execute('SELECT username FROM PETUGAS_FASKES WHERE username not in (SELECT username_petugas FROM FASKES);')
    context_2 = {
        'username' : dictfetchall(cursor)
    }

    temp_user = set()

    username = {}
    for k in context_2['username']:
        for l in k:
            temp_user.add(k['username'])

    temp_user = sorted(temp_user)

    username = {
        'username':temp_user
    }

    cursor.execute('SELECT Kode FROM TIPE_FASKES;')
    context_3 = {
        'kode' : dictfetchall(cursor)
    }
    temp_kode_tipe_faskes = set()

    kode_tipe_faskes = {}
    for m in context_3['kode']:
        for n in m:
            temp_kode_tipe_faskes.add(m['kode'])

    temp_kode_tipe_faskes = sorted(temp_kode_tipe_faskes)

    kode_tipe_faskes = {
        'kode' : temp_kode_tipe_faskes
    }

    id_lokasi.update(username)
    id_lokasi.update(kode_tipe_faskes)


    return render(request, 'cek_lokasi/add_faskes.html', id_lokasi)


def perantara(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')

    arg = ''
    if request.method == 'GET':
        if 'provinsi' in request.GET:
            arg = request.GET['provinsi'].lower()
  
    cursor.execute(f'''
        SELECT 
            *
        FROM LOKASI
        WHERE 
            provinsi = '{arg}'
        ;''')
    
    print(arg)
 

    data = json.dumps(dictfetchall(cursor))
    print(data)
    
    return JsonResponse(data, safe=False)

def perantara_id(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')
    arg = ''
    if request.method == 'GET':
        if 'id' in request.GET:
            arg = request.GET['id']

    cursor.execute(f'''
        SELECT 
            *
        FROM LOKASI
        WHERE 
            id = '{arg}'
        ;''')
    
    # print(arg)
 

    data = json.dumps(dictfetchall(cursor))
    print(data)
    
    return JsonResponse(data, safe=False)

def perantara_kode(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')
    arg = ''
    if request.method == 'GET':
        if 'kode' in request.GET:
            arg = request.GET['kode']
    
    print(arg)

    cursor.execute(f'''
        SELECT 
            Nama_Tipe
        FROM TIPE_FASKES
        WHERE 
            kode = '{arg}'
        ;''')
    
    # print(arg)
 

    data = json.dumps(dictfetchall(cursor))
    # print(data)
    
    return JsonResponse(data, safe=False)

'''
Trigger no. 5
1. CRUD Fasilitas Kesehatan
    - (CRUD) Admin Satgas
    - (R) Supplier

2. CRUD Warehouse Satgas
    - (CRD) Admin Satgas
    - (R) Supplier    

3. Create Batch Pengiriman (Penentuan
lokasi asal dan tujuan pengiriman)
    - Admin Satgas

'''
@is_user_role_correct(["ADMIN_SATGAS", "SUPPLIER"])
def insert_to_faskes_table(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')
    
    if request.method == 'POST':
        # is form.is_valid():
        form = request.POST
        print(form)

        tambah_faskes(form)
        return redirect ('cek_lokasi:insert_to_faskes_table')

    cursor.execute(f'''
        SELECT
            fk.kode_faskes_nasional as kode_faskes,
            lk.provinsi as provinsi,
            lk.kabkot as kabupaten,
            lk.kecamatan as kecamatan,
            lk.kelurahan as kelurahan,
            lk.jalan_no as jalan_no,
            fk.username_petugas as petugas_faskes,
            tfk.nama_tipe as tipe_faskes
        FROM
            FASKES fk,
            TIPE_FASKES tfk,
            LOKASI lk
        WHERE
            tfk.kode = fk.kode_tipe_faskes AND
            fk.id_lokasi = lk.id
    ;''')

    rows = dictfetchall(cursor)

    if request.session.get('role') == "ADMIN_SATGAS":
        role = "admin_satgas"

    else: 
        role = "others"

    user_role = request.session.get('user_role')
    username = request.session.get('username')

    # print(user_role)

    
        
    return render(request, 'cek_lokasi/daftar_faskes.html', {'rows' : rows, 'user_role' : user_role, 'username' : username })

'''
Trigger no. 5
1. CRUD Fasilitas Kesehatan
    - (CRUD) Admin Satgas
    - (R) Supplier

2. CRUD Warehouse Satgas
    - (CRD) Admin Satgas
    - (R) Supplier    

3. Create Batch Pengiriman (Penentuan
lokasi asal dan tujuan pengiriman)
    - Admin Satgas

'''
@is_user_role_correct(["ADMIN_SATGAS", "SUPPLIER"])
def insert_to_ws_table(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')
    
    if request.session.get('role') == "ADMIN_SATGAS":
        if request.method == 'POST':
            # is form.is_valid():
            form = request.POST
            # print(form)

            tambah_ws(form)
            return redirect ('cek_lokasi:insert_to_ws_table')

    cursor.execute(f'''
        SELECT
            lk.provinsi as provinsi,
            lk.kabkot as kabupaten,
            lk.kecamatan as kecamatan,
            lk.kelurahan as kelurahan,
            lk.jalan_no as jalan_no
        FROM
            WAREHOUSE_PROVINSI wp,
            LOKASI lk
        WHERE
            wp.id_lokasi = lk.id
    ;''')

    rows = dictfetchall(cursor)
    # print(rows)

    user_role = request.session.get('user_role')
    username = request.session.get('username')

    context = {
        'rows' : rows,
        'user_role' : user_role,
        'username' : username,
    }


    count = 1
    for i in range (len(rows)):

        context['no'] = count 
        count = count + 1
    # print(context)

    return render(request, 'cek_lokasi/daftar_warehouse_satgas.html',{'username' : username, 'rows' : rows, 'user_role' : user_role})

@is_user_role_correct(["ADMIN_SATGAS"])
def delete_faskes(request, kode_faskes):
    faskes_delete(kode_faskes)
    return redirect('cek_lokasi:insert_to_faskes_table')

# delete warehoude_satgas
@is_user_role_correct(["ADMIN_SATGAS"])
def delete_ws(request, provinsi,kabupaten, kecamatan, kelurahan, jalan_no):
    id_lokasi = get_id_lokasi(provinsi, kabupaten, kecamatan, kelurahan, jalan_no)
    ws_delete(id_lokasi[0]['id'])
    return redirect('cek_lokasi:insert_to_ws_table')


@is_user_role_correct(["ADMIN_SATGAS"])
def faskes_update(request):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')

    cursor.execute('SELECT Id FROM LOKASI;')
    
    context = {
        'id': dictfetchall(cursor)
    }

    temp_id = set()

    id_lokasi = {}
    for i in context['id']:
        for j in i:
            temp_id.add(i['id'])
        
    temp_id = sorted(temp_id)

    id_lokasi = {
        'id' : temp_id
    }

    cursor.execute('SELECT username FROM PETUGAS_FASKES WHERE username not in (SELECT username_petugas FROM FASKES);')
    context_2 = {
        'username' : dictfetchall(cursor)
    }

    temp_user = set()

    username = {}
    for k in context_2['username']:
        for l in k:
            temp_user.add(k['username'])

    temp_user = sorted(temp_user)

    username = {
        'username':temp_user
    }

    cursor.execute('SELECT Kode FROM TIPE_FASKES;')
    context_3 = {
        'kode' : dictfetchall(cursor)
    }
    temp_kode_tipe_faskes = set()

    kode_tipe_faskes = {}
    for m in context_3['kode']:
        for n in m:
            temp_kode_tipe_faskes.add(m['kode'])

    temp_kode_tipe_faskes = sorted(temp_kode_tipe_faskes)

    kode_tipe_faskes = {
        'kode' : temp_kode_tipe_faskes
    }

    id_lokasi.update(username)
    id_lokasi.update(kode_tipe_faskes)

    return render(request, 'cek_lokasi/add_faskes.html', id_lokasi)





    








