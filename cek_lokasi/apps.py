from django.apps import AppConfig


class CekLokasiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cek_lokasi'
