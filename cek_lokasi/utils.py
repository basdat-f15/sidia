from django.db import connection

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]

    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def tambah_faskes(form):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')
    
    cursor.execute(f'''
        INSERT INTO FASKES VALUES (
             ('{form['kode_faskes']}', 
             '{form['id']}',
             '{form['petugas-faskes']}',
             '{form['kode']}'
        );
    ''')

def tambah_ws(form):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')
    
    cursor.execute(f'''
        INSERT INTO FASKES VALUES (
             ('{form['id_lokasi']}'
        );
    ''')

def faskes_query_admin(user_role):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')

    cursor.execute(f'''
        SELECT
            fk.kode_faskes_nasional,
            lk.provinsi,
            lk.kabkot,
            lk.kecamatan,
            lk.kelurahan,
            lk.jalan_no,
            fk.username_petugas,
            fk.kode_tipe_faskes
        FROM
            FASKES fk,
            LOKASI lk
        WHERE
            lk.id = fk.id_lokasi
    ;''')

    return dictfetchall(cursor)


def faskes_delete(kode_faskes):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')

    cursor.execute(f'''
        DELETE FROM 
            FASKES
        WHERE
            kode_faskes_nasional = '{kode_faskes}'
    ;''')

def get_id_lokasi(provinsi, kabupaten, kecamatan, kelurahan, jalan_no):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')

    cursor.execute(f'''
        SELECT
            id
        FROM
            LOKASI
        WHERE
            provinsi = '{provinsi}' AND
            kabkot = '{kabupaten}' AND 
            kecamatan = '{kecamatan}' AND
            kelurahan = '{kelurahan}' AND
            jalan_no = '{jalan_no}'

    ''')
    id_data = dictfetchall(cursor)
    return id_data
    
def ws_delete(id_lokasi):
    cursor = connection.cursor()
    cursor.execute('SET SEARCH_PATH TO SIDIA;')
    
    cursor.execute(f'''
        DELETE
        FROM
            WAREHOUSE_PROVINSI
        WHERE
            id_lokasi = '{id_lokasi}'
    ;''')

